<?php 
include_once "conexion.php";
$cadena = "select idarticulo,titulo,resumen,imagen,autor,cuerpo,fecha from articulo where idarticulo='".$_GET['id']."'";
$resultado = $conex->query($cadena);
$row = $resultado->fetch_assoc();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include_once "head.php";?>
</head>
<body>
   <?php include_once "nav.php";?>

  <!--Pagina de contenido-->
  <main>
    <div class="container">
      <div class="row">

        <!--Entrada de Post-->
        <div class="col-lg-9">

          <!--Titulo Author-->
          <h1 class="mb-4"><?php echo $row['titulo'];?></h1>
          <p class="lead">
            by <a href="#"><?php echo $row['autor'];?></a>
          </p>
          <hr>
          <p>Posted on January 1, 2018 at 12:00 pm</p>
          <hr>
          <img class="img-fluid rounded" src="img/posts/<?php echo $row['imagen'];?>" alt="">
          <hr>
          <!--/Titulo Author-->

          <!--Post Content-->
          <p class="lead">
            <?php echo $row['cuerpo'];?>
          </p>
          <hr>

          <!--Comments Form-->
          <div class="card mb-4">
            <h5 class="card-header">Leave a Comment:</h5>
            <div class="card-body">
              <form>
                <div class="form-group">
                  Nombre: <input type="text" name="nombre" class="form-control">
                </div>
                <div class="form-group">
                  Comentario:
                  <textarea class="form-control" rows="3"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
              </form>
            </div>
          </div>
          <!--/Comments Form-->

          <!--Single Comment-->
          <div class="media mb-4">
            <img src="http://placehold.it/50x50" alt="" class="d-flex mr-3 rounded-circle">
            <div class="media-body">
              <h5 class="mt-0">Commenter Name</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputa at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
          </div>
          <!--/Single Comment-->

          <!--Single Comment-->
          <div class="media mb-4">
            <img src="http://placehold.it/50x50" alt="" class="d-flex mr-3 rounded-circle">
            <div class="media-body">
              <h5 class="mt-0">Commenter Name</h5>
              Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin. Cras purus odio, vestibulum in vulputa at, tempus viverra turpis. Fusce condimentum nunc ac nisi vulputate fringilla. Donec lacinia congue felis in faucibus.
            </div>
          </div>
          <!--/Single Comment-->          

          <!--/Post Content-->

        </div>
        <!--/Entrada de Post-->

        <!--Sidebar derecho-->
        <div class="col-lg-3">
          
          <!--Post recientes-->
          <div class="card mb-4">
            <h5 class="card-header">Recent post</h5>
            <div class="card-body">
              <ul class="list-unstyled mb-0">
                <li><a href="#">Post Title 1</a></li>
                <li><a href="#">Post Title 2</a></li>
                <li><a href="#">Post Title 3</a></li>
              </ul>
            </div>
          </div>
          <!--/Post recientes-->

          <!--Archivos-->
          <div class="card mb-4">
            <h5 class="card-header">Archivos</h5>
            <div class="card-body">
              <ul class="list-unstyled mb-0">
                <li><a href="#">Enero 2018</a></li>
                <li><a href="#">Diciembre 2017</a></li>
              </ul>
            </div>
          </div>
          <!--/Archivos-->

          <!--Side Widget-->
          <div class="card mb-4">
            <h5 class="card-header">Acerca de mi</h5>
            <div class="card-body">
              You can put anythig you want inside of these side widgets. They are easy to use, and feature the new Boostrap 4 card containers.!
            </div>
          </div>
          <!--/Side Widget-->

        </div>
        <!--/Sidebar derecho-->
      </div>
    </div>

  </main>
  <!--/Pagina de contenido-->

  <?php include_once "footer.php";?>

  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>

</body>
</html>
