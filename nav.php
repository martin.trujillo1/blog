<!--Header-->
<header>

<!--Navbar-->
<nav class="navbar navbar-dark navbar-expand-lg bg-primary scrolling-navbar">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item"><a href="index.php" class="nav-link">Inicio</a></li>
      <li class="nav-item active"><a href="blog.php" class="nav-link">Blog</a></li>
      <li class="nav-item"><a href="contacts.php" class="nav-link">Contactenos</a></li>
    </ul>
    <ul class="navbar-nav nav-flex-icons">
      <li class="nav-item">
        <a href="#" class="nav-link"><i class="fab fa-facebook-f"></i></a>
      </li>
      <li class="nav-item">
        <a href="#" class="nav-link"><i class="fab fa-twitter"></i></a>
      </li>
    </ul>
  </div>
</nav>
<!--/Navbar-->  

  <!--Carrousel-->
  <div id="carouselExampleControls" class="carousel slide carousel-fade" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleControls" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleControls" data-slide-to="1"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/header.jpg" alt="" class="d-block w-100">
        <div class="carousel-caption d-none d-md-block text-left text-dark">
          <h5><strong>&nbsp;</strong></h5>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/principal.jpg" alt="" class="d-block w-100">
        <div class="carousel-caption d-none d-md-block text-left">
          <h5><strong>&nbsp;</strong></h5>
        </div>          
      </div>
    </div>
    <a href="#carouselExampleControls" class="carousel-control-prev" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a href="#carouselExampleControls" class="carousel-control-next" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <!--/Carrousel-->
</header>
<!--/Header-->