<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once "head.php";?>
</head>
<body>
  <?php include_once "nav.php";?>

  <!--Pagina de contenido-->
  <section class="container">
    <h2 class="h1-responsive font-weight-bold text-center mb-4">Contactenos</h2>
    <p class="text-center w-responsive mx-auto mb-4">Do you have any question?. Ploease do not hesiate to contact us directly. Our twam wil come back to you within a matter of hours to help you.</p>
    <div class="row">
      <div class="col-lg-9">
        <form>
          <div class="row">
            <div class="col">
              <input type="text" placeholder="Nombres" required="required" class="form-control mb-4">
              <input type="text" placeholder="E-mail" required="required" class="form-control mb-4">
              <input type="text" placeholder="Phone" required="required" class="form-control mb-4">
            </div>
            <div class="col">
              <textarea placeholder="Message" class="form-control mb-4" rows="6"></textarea>
              <input type="submit" value="Enviar" class="btn btn-success">
            </div>
          </div>
        </form>
      </div>  
      <div class="col-lg-3 text-center">
        <ul class="list-unstyled">
          <li>
            <i class="fas fa-map-marker-alt fa-2x"></i>
            <p>San Francisco, CA 94126, USA</p>
          </li>
          <li>
            <i class="fas fa-phone mt-4 fa-2x"></i>
            <p>+01 234 567 89</p>
          </li>
          <li>
            <i class="fas fa-envelope mt-4 fa-2x"></i>
            <p>contactenos@mdboostrap.com</p>
          </li>
        </ul>
      </div>  
    </div>

    <h3 class="head">Encuentranos</h3>
    <div class="row">
      <iframe src="https://maps.google.co.in/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Lighthouse+Point,+FL,+United+States&amp;aq=4&amp;oq=light&amp;sll=26.275636,-80.087265&amp;sspn=0.04941,0.104628&amp;ie=UTF8&amp;hq=&amp;hnear=Lighthouse+Point,+Broward,+Florida,+United+States&amp;t=m&amp;z=14&amp;ll=26.275636,-80.087265&amp;output=embed" width="100%" height="300" id="mapa" class="mb-4" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
    </div>
  </section>
  <!--/Pagina de contenido-->

  <?php include_once "footer.php";?>

  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>
</body>
</html>