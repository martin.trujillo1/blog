<?php  
include_once "conexion.php";

function formato_fecha($fecha){
  $arrFecha = explode("-",substr($fecha,0,10)); 
  $arrMese = ["Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Setiembre","Octubre","Noviembre","Diciembre"];
  $anio = $arrFecha[0];
  $mes  = $arrFecha[1];
  $dia  = $arrFecha[2];
  return $dia." ".$arrMese[(int)$mes-1]." ".$anio;
}

$cadena = "select idarticulo,titulo,resumen,imagen,autor,cuerpo,fecha from articulo order by fecha desc";
$resultado = $conex->query($cadena);
$fila = "";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once "head.php";?>
</head>
<body>
  <?php include_once "nav.php";?>
  <!--Pagina de contenido-->
  <main>
    <div class="container">
      <div class="row">

        <!--Entrada de Post-->
        <div class="col-lg-9">

          <?php
          while($row=$resultado->fetch_assoc()){
            ?>
            <!--Blog post-->
            <div class="card mb-4">
              <div class="card-body">
                <h2 class="card-title"><a href="posts.php?id=<?php echo $row['idarticulo'];?>"><?php echo $row['titulo']?></a></h2>
                <p class="card-text"><?php echo $row['resumen']; ?></p>
                <a href="posts.php?id=<?php echo $row['idarticulo'];?>">Leer mas</a>
              </div>
              <div class="card-footer">
                Publicado por <a href="#"><?php echo $row['autor'];?></a> el <?php echo formato_fecha($row['fecha']);?>
              </div>
            </div>
            <!--/Blog post-->
            <?php
          }
          ?>

        </div>
        <!--/Entrada de Post-->

        <!--Sidebar derecho-->
        <div class="col-lg-3">
          
          <!--Post recientes-->
          <div class="card mb-4">
            <h5 class="card-header">Recent post</h5>
            <div class="card-body">
              <ul class="list-unstyled mb-0">
                <li><a href="#">Post Title 1</a></li>
                <li><a href="#">Post Title 2</a></li>
                <li><a href="#">Post Title 3</a></li>
              </ul>
            </div>
          </div>
          <!--/Post recientes-->

          <!--Archivos-->
          <div class="card mb-4">
            <h5 class="card-header">Archivos</h5>
            <div class="card-body">
              <ul class="list-unstyled mb-0">
                <li><a href="#">Enero 2018</a></li>
                <li><a href="#">Diciembre 2017</a></li>
              </ul>
            </div>
          </div>
          <!--/Archivos-->

          <!--Side Widget-->
          <div class="card mb-4">
            <h5 class="card-header">Acerca de mi</h5>
            <div class="card-body">
              You can put anythig you want inside of these side widgets. They are easy to use, and feature the new Boostrap 4 card containers.!
            </div>
          </div>
          <!--/Side Widget-->

        </div>
        <!--/Sidebar derecho-->
      </div>
    </div>

  </main>
  <!--/Pagina de contenido-->

  <?php include_once "footer.php";?>

  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>

</body>
</html>
