<footer class="page-footer font-small primary-color pt-4">
  <div class="container-fluid text-center text-md-left">
    <div class="row">
      <div class="col mt-md-0 mt-3">
        <h5 class="text-uppercase font-weight-bold">EMPRESA</h5>
        <ul class="list-unstyled">
          <li><a href="#">Telefono: (0051) 99775-9824</a></li>
          <li><a href="#">Calle Corpancho 180 Of.404C - Surco</a></li>
          <li>E-mail: <a href="mailto:informes@empresa.com"></a>empresa@empresa.com</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright text-center py-3">
    © 2020 Copyright:
    <a href="#"> EMPRESA</a>
  </div>
</footer>