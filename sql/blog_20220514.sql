/*
SQLyog Community v13.1.7 (64 bit)
MySQL - 10.5.9-MariaDB : Database - blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `blog`;

/*Table structure for table `articulo` */

DROP TABLE IF EXISTS `articulo`;

CREATE TABLE `articulo` (
  `idarticulo` int(11) NOT NULL AUTO_INCREMENT,
  `idcategoria` int(11) DEFAULT 0,
  `idusuario` int(11) DEFAULT NULL,
  `titulo` varchar(250) DEFAULT NULL,
  `resumen` varchar(250) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `autor` varchar(150) DEFAULT NULL,
  `cuerpo` text DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  PRIMARY KEY (`idarticulo`),
  KEY `FK_articulo_categoria` (`idcategoria`),
  KEY `FK_articulo_usuario` (`idusuario`),
  CONSTRAINT `FK_articulo_categoria` FOREIGN KEY (`idcategoria`) REFERENCES `categoria` (`idcategoria`),
  CONSTRAINT `FK_articulo_usuario` FOREIGN KEY (`idusuario`) REFERENCES `usuario` (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `articulo` */

insert  into `articulo`(`idarticulo`,`idcategoria`,`idusuario`,`titulo`,`resumen`,`imagen`,`autor`,`cuerpo`,`fecha`) values 
(1,1,1,'El arte digital y sus infinitas posibilidades','El arte digital es, seguramente, la disciplina más novedosa dentro del mundo de las artes plásticas. Muchos la consideran la expresión artística del futuro por las nuevas   posibilidades creativas con la que, cada día, es capaz de sorprendernos. Las ','foto1.jpg',NULL,'El arte digital es, seguramente, la disciplina más novedosa dentro del mundo de las artes plásticas. Muchos la consideran la expresión artística del futuro por las nuevas   posibilidades creativas con la que, cada día, es capaz de sorprendernos.\r\nLas nuevas tecnologías se ponen al servicio del arte interconectando sectores muy diversos que abarcan desde la imagen y la fotografía tradicional hasta los más espectaculares efectos especiales. Arte digital puede ser un cuadro en el que casi podemos sumergirnos gracias a su diseño en 3D, un videojuego en el que participamos de manera activa junto a personajes virtuales, un corto de animación, una fotografía en la que nada es lo que parece… la creatividad no tiene límites y con las herramientas actuales puede llegar a donde se proponga.\r\nTambién el propio concepto de artista ha cambiado radicalmente. A las técnicas tradicionales de dibujo, pintura o escultura, basadas principalmente en la representación de la realidad, se han unido conceptos rompedores como el modelado de escenarios y personajes, los objetos “interactivos”, los entornos virtuales o las interfaces gráficas que han supuesto una verdadera revolución. Podría decirse que el campo del arte y el de la comunicación se han unido definitivamente abriendo nuevas vías de expresión, aún inexploradas, que tienen en cuenta los nuevos soportes: pcs, consolas, tablets, Iphones…\r\nSin duda, éste el motivo principal por el que también la formación relacionada con el Arte ha cambiado, ampliando horizontes y ofreciendo nuevas opciones que no se limitan, únicamente, al Grado en Bellas Artes. Novedosos estudios como el Grado en Diseño, Animación y Arte Digital son un buen ejemplo de cómo aprender a combinar la propia creatividad con las nuevas tecnologías.\r\nSe trata de un grado que incluye materias muy diversas: fotografía, modelaje, diseño 3D, narrativa audiovisual, comunicación, game desing y por supuesto, Historia del Arte una asignatura que no puede faltar como fuente de inspiración para los artistas del siglo XXI.\r\nHay que tener en cuenta que el arte digital está muy presente en nuestro día a día (no solo en una exposición puntual que puedas ver en un museo). Basta pensar, por ejemplo, en el mundo de la publicidad y el marketing, con anuncios, campañas, banners, carteles, cortos… nadie puede negar que algunas piezas son realmente obras de arte. Lo mismo ocurre con los actuales videojuegos, que no dejan de sorprender por su impactante realismo, por la calidad en el diseño detallado de personajes y “mundos” y por la capacidad de interactuación que permiten.\r\nProductores digitales, animadores en 2D y 3D, responsables de post producción, expertos en efectos especiales, creativos publicitarios, diseñadores de contenidos audiovisuales… nuevas profesiones que hacen de un trabajo basado en la tecnología, un verdadero arte.','2008-05-12 00:00:00'),
(2,1,1,'Slate de HP, la competencia de iPad','HP ha presentado su nueva joya, Slate. Aun se desconoce cuando llegará a España, y el precio por el cual podremos adquirir este dispositivo. Pero si sabemos que podremos comprarlo en su página web o bien por los medios de canal convencionales, y es q','foto2.jpg',NULL,'HP ha presentado su nueva joya, Slate. Aun se desconoce cuando llegará a España, y el precio por el cual podremos adquirir este dispositivo. Pero si sabemos que podremos comprarlo en su página web o bien por los medios de canal convencionales, y es que HP sigue siendo fiel a su sistema de distribución y no tiene intención de cambiarlo. Abrir una división de venta paralela de dispositivos ajena al canal de distribución sería un error y enfrentaría a los Partners con HP. Algo que es mejor evitar.\r\nEl Slate de HP nos recuerda mucho al nuevo iPad de Apple, y es que parece que han tomado ejemplo del nuevo dispositivo de Apple. Aunque el director de marketing comenta que no tiene nada que ver y no será un producto para sustituir a otros sino que abre una vía de negocio nueva.\r\nSlate de HP vendrá con Windows 7 como sistema operativo y podrá leer flash, cosa que iPad no puede. Y esto será un punto positivo para Slate. Pretende ser un dispositivo con un alto rendimiento multimedia y orientado al ocio, descartando así que sea un producto de gama profesional.','2008-07-12 00:00:00'),
(13,1,1,'Los elegantes','','imagen','Pedro','fdsfsdf','2020-07-02 14:30:26'),
(14,1,1,'Los elegantes','','imagen','Pedro','tutyu','2020-07-02 14:31:48'),
(15,1,1,'Los animales','','4262af504f35884ad23a9951008663','Shakespeare','Elementos a todos ','2020-07-02 14:48:23'),
(16,1,1,'Los animales','','41c5d9fa9410027d3a1a9578b004ablogo_peru_grande.png','Shakespeare','Despertad','2020-07-02 14:49:05'),
(17,1,1,'Los Perez','','7c641beac1467b427b9fd4c266bde7LOGO ROLL.png','Imagen','sdfsdfsdf','2020-07-02 14:51:04'),
(18,1,1,'Los Perez','','aafb3b17b792063fcc317cc2299f81logoperugrandetotal.png','Pedro','asdfas fsadfsd  fwerwr','2020-07-02 14:53:19'),
(19,1,1,'Los Nazis','Todos sogre mi madre','e32fbc1b8fccf5c8ea5cd113624305LOGOROLL.png','Perez','lorem loremloremloremloremlorem lorem loremloremloremloremlorem lorem loremloremloremloremlorem lorem loremloremloremloremlorem lorem loremloremloremloremlorem','2020-07-02 15:04:08');

/*Table structure for table `categoria` */

DROP TABLE IF EXISTS `categoria`;

CREATE TABLE `categoria` (
  `idcategoria` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(255) DEFAULT '0',
  PRIMARY KEY (`idcategoria`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `categoria` */

insert  into `categoria`(`idcategoria`,`descripcion`) values 
(1,'Electronica');

/*Table structure for table `comentario` */

DROP TABLE IF EXISTS `comentario`;

CREATE TABLE `comentario` (
  `idcomentario` int(11) NOT NULL AUTO_INCREMENT,
  `idarticulo` int(11) DEFAULT NULL,
  `descripcion` text CHARACTER SET latin1 DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `nombre` varchar(15) CHARACTER SET latin1 DEFAULT '',
  PRIMARY KEY (`idcomentario`),
  KEY `FK_ARTICULO_COMENTARIO_idx` (`idarticulo`),
  CONSTRAINT `FK_ARTICULO_COMENTARIO` FOREIGN KEY (`idarticulo`) REFERENCES `articulo` (`idarticulo`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8;

/*Data for the table `comentario` */

insert  into `comentario`(`idcomentario`,`idarticulo`,`descripcion`,`fecha`,`nombre`) values 
(1,1,'Amigo estoy tratando de instalar el apache en el Ubuntu, como hago esto','2007-12-06 00:00:00','Ricardo'),
(2,1,'No puedes escribir mas???','2007-12-02 00:00:00','Ruben'),
(3,1,'Una consulta, como utilizo los procesos crond\r\nSaludos','2008-02-05 00:00:00','carlos'),
(4,1,'afds','2008-03-31 00:00:00','f'),
(5,2,'afds','2008-03-31 00:00:00','adf'),
(6,2,'afds','2008-03-31 00:00:00','adf'),
(7,2,'afds','2008-03-31 00:00:00','adf'),
(8,2,'afds','2008-03-31 00:00:00','adf');

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nombres` varchar(255) DEFAULT NULL,
  `apellidos` varchar(255) DEFAULT NULL,
  `estado` int(11) DEFAULT 0,
  PRIMARY KEY (`idusuario`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

/*Data for the table `usuario` */

insert  into `usuario`(`idusuario`,`login`,`password`,`nombres`,`apellidos`,`estado`) values 
(1,'admin','e10adc3949ba59abbe56e057f20f883e','Martín','Trujillo',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
