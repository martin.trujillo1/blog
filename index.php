<?php 
$script = "";

function RandomToken($length = 32){
    if(!isset($length) || intval($length) <= 8 ){
      $length = 32;
    }
    if (function_exists('random_bytes')) {
        return bin2hex(random_bytes($length));
    }
    if (function_exists('mcrypt_create_iv')) {
        return bin2hex(mcrypt_create_iv($length, MCRYPT_DEV_URANDOM));
    }
    if (function_exists('openssl_random_pseudo_bytes')) {
        return bin2hex(openssl_random_pseudo_bytes($length));
    }
}

if(!empty($_POST)){
  include_once "conexion.php";
  extract($_POST, EXTR_PREFIX_ALL,"p");

  if($_FILES['imagen']['size']>5*1024*1024){
    $script = "alert('Su imagen es demasiado grande, busque otra imagen.')";
  }
  else{
    $archivo = RandomToken(15);
    $imagen  = $archivo.str_replace(" ","",$_FILES['imagen']['name']);
    move_uploaded_file($_FILES['imagen']['tmp_name'], 'img/posts/'.$imagen);
    $cadena = "insert into articulo values (DEFAULT,1,1,'".$p_titulo."','".$p_resumen."','".$imagen."','".$p_autor."','".$p_cuerpo."',NOW())";
    $conex->query($cadena);
    header("Location:blog.php");
  }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <?php include_once "head.php";?>
</head>
<body>
  <?php include_once "nav.php";?>
  <section class="container">
    <div class="row">
      <h2 class="text-center w-100">Agregar articulo</h2>
    </div>
    <form method="post" enctype="multipart/form-data">
    <div class="row">
        <div class="col-lg-6">
          <div class="form-group">
            <input type="text" name="titulo" id="titulo" class="form-control" maxlength="150" placeholder="Titulo del artículo">
          </div>
          <div class="form-group">
            <input type="text" name="autor" id="autor" class="form-control" maxlength="150" placeholder="Autor del artículo">
          </div>
          <div class="form-group">
            <input type="text" name="resumen" id="resumen" class="form-control" maxlength="150" placeholder="Resumen del artículo">
          </div>          
        </div>
        <div class="col-lg-6">
          <div class="form-group">
            <label for="imagen">Adjunte imagen del articulo</label>
            <input type="file" required="required" name="imagen" id="imagen" accept="img/jpg">
          </div> 
        </div>
     </div>      
      <div class="row">
        <div class="col-lg-12">
          <div class="form-group">
            <textarea required="required" placeholder="Descripcion del artículo" name="cuerpo" id="cuerpo" class="form-control" maxlength="450" rows="5" placeholder="Ingrese contenido"></textarea>
          </div>
          <div class="form-group text-center">
            <button class="btn btn-success">Subir</button>
          </div>
        </div>
      </div>
    </form>
  </section>

  <?php include_once "footer.php";?>

  <script type="text/javascript"><?php echo $script;?></script>

  <!-- jQuery -->
  <script type="text/javascript" src="js/jquery.min.js"></script>
  <!-- Bootstrap tooltips -->
  <script type="text/javascript" src="js/popper.min.js"></script>
  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="js/bootstrap.min.js"></script>
  <!-- MDB core JavaScript -->
  <script type="text/javascript" src="js/mdb.min.js"></script>
  <!-- Your custom scripts (optional) -->
  <script type="text/javascript"></script>  

</body>
</html>